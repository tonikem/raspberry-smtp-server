from time import sleep
from copy import deepcopy
from datetime import datetime
from src.kaavio import *
from src.tietokanta_yhteys import *
from src.email_palvelu import send_email
from src.serial_yhteys import get_serial_data

file = open('app.yml', 'r')
muuttujat = yaml.load(file, Loader=yaml.FullLoader)
READY_TEMP = float(muuttujat['anturi']['ready-temp'])
ILMOITUS_TIME = float(muuttujat['anturi']['ilmoitus-time'])
file.close()


def get_temp_float(_temp):
    return float(_temp.replace(',', '.')\
        .replace('C', '')\
        .replace('F', '')\
        .strip())


average_time_left_map = {
    # Lämpötilastepit valmistumisajan arvioimiseen
    15.0: None, 17.5: None, 20.0: None, 22.5: None,
    25.0: None, 27.5: None, 30.0: None, 32.5: None,
    35.0: None, 37.5: None, 40.0: None, 42.5: None,
    45.0: None, 47.5: None, 50.0: None, 52.5: None,
    55.0: None, 57.5: None, 60.0: None, 62.5: None,
    65.0: None, 67.5: None, 70.0: 0
    # Tavoitelämpötila on 70.0 Celsius-astetta.
}


def populate_avergage_time_map(initial_data):
    current_kerta, target_temp_time = None, None
    listatut_arvot = deepcopy(average_time_left_map)
    for k in listatut_arvot.keys():
        listatut_arvot[k] = []

    temp_steps = list(average_time_left_map.keys())
    temp_steps.sort(reverse=True)
    current_step_index = 0
    current_step = temp_steps[current_step_index]

    for data in initial_data:
        if current_kerta is None:
            current_kerta = data[1]
            target_temp_time = data[2]

        if data[1] != current_kerta:
            current_kerta = data[1]
            target_temp_time = data[2]
            current_step_index = 0
            current_step = temp_steps[current_step_index]

        temp = get_temp_float(data[3])

        if current_step and temp < current_step:
            time_left = float(target_temp_time) - float(data[2])
            listatut_arvot[current_step].append(time_left)
            current_step_index += 1
            current_step = temp_steps[current_step_index]

    for step, values in listatut_arvot.items():
        if len(values) > 0:
            average = sum(values) / len(values)
            average_time_left_map[step] = average


def main():
    sauna_valmistumassa, sauna_valmis = False, False
    testi_index = 0
    # Avataan tietokantayhteys:
    init_db_connection()
    # Käynnistetään käyttöliittymä:
    initial_data = get_pageable_rows(0)
    populate_avergage_time_map(initial_data)

    kerta = 1
    if len(initial_data) > 0:
        kerta = initial_data[0][1] + 1
    start_interface(initial_data, kerta, average_time_left_map)

    while True:
        try:
            data = get_serial_data()
            # data = testi_arvot[testi_index].split(';')
            # testi_index += 1
            # sleep(0.3)

            temp = get_temp_float(data[0])

            # Päivitetään lämpötila käyttöliittymälle:
            shared_data['current-temp'] = temp

            temp_step = 2.5 * round(temp / 2.5)
            average_time_left = average_time_left_map[temp_step]

            if average_time_left and not sauna_valmistumassa:
                if average_time_left <= ILMOITUS_TIME:
                    sauna_valmistumassa = True
                    Thread(target=send_email,
                           args=(data, "Pian valmis", average_time_left)
                           ).start()

            elif temp >= READY_TEMP and not sauna_valmis:
                sauna_valmis = True
                Thread( target=send_email,
                    args=(data, "Sauna Valmis", 0)
                ).start()

                sleep(5)  # Nopeutettu demoa varten

                # data[1] = "2 tunti 10 minuuttia"
                Thread( target=send_email,
                    args=(data, "Sauna yliajalla", None)
                ).start()

            elif True:
                pass

            # Tallennetaan viimeisin mittaus kantaan:
            # save_to_db(time.time(), data, kerta)

        except IndexError:
            break
        except Exception as e:
            # print(e)
            pass


if __name__ == '__main__':
    main()
    # Poistumiskomento käyttöliittymälle:
    # stop_interface_thread()




