from src.tietokanta_yhteys import init_db_connection, save_to_db


def kirjoita_kantaan(_kerta):
    with open("mock-data.csv", "r") as file:
        for line in file:
            data = line.strip().split(';')
            values = (data[1], data[2], '...', '...')
            save_to_db(data[0], values, _kerta)


def main():
    init_db_connection()
    for lammitys_kerta in range(1, 12):
        kirjoita_kantaan(lammitys_kerta)
    print("Mock-data ajettu kantaan.")


# main()


