import array
from copy import deepcopy
from datetime import datetime
import sys
import random
import time
import PySimpleGUI as sg
from threading import Thread, Lock
from src.sauna_utils import *
from src.tietokanta_yhteys import get_all_rows
from src.tietokanta_yhteys import get_pageable_rows

STEP_SIZE = 1
SAMPLES = 100
SAMPLE_MIN = 0
SAMPLE_MAX = 120
CANVAS_SIZE = (1000, 600)
LABEL_SIZE = (1000, 20)
shared_data = {
    'current-temp': 0.00,
    'previews-temp': 0.00
}
interface_thread_lock = Lock()
main_thread_state = 'running'
current_page = 0


def stop_interface_thread():
    global main_thread_state
    main_thread_state = 'exit'


def graph_event_loop(initial_data, lammitys_kerta, average_time_left_map):
    global main_thread_state, current_page
    taulu_headings = [' _id ', 'Lämmityskerta', 'Aika', 'Lämpötila', 'kulunut aika']
    filtered_data = []
    for data in initial_data:
        new_data = list(deepcopy(data))
        new_data[2] = datetime\
            .fromtimestamp(float(data[2]))\
            .strftime('%Y-%m-%d %H:%M:%S')
        filtered_data.append(new_data)

    pt_values, pt_times = [], []
    for index in range(SAMPLES + 1):
        pt_values.append("")
        pt_times.append("")

    timebar = sg.Graph(LABEL_SIZE, (0, 0), (SAMPLES, 20), background_color='white', key='times')
    graph = sg.Graph(CANVAS_SIZE, (0, 0), (SAMPLES, SAMPLE_MAX), background_color='black', key='graph')
    layout = [
        [sg.Quit(button_color=('white', 'orangered')),
         sg.Button('View saved values', button_color=('white', 'orangered')),
         sg.Text('Lämpötila:'),
         sg.Text(7 * ' ', key='output'),
         sg.Text('°C'), sg.Text('|'),
         sg.Text('Lämmityskerta: '),
         sg.Text(lammitys_kerta),
         sg.Text('|'), sg.Text('Arvioitu valmistumisaika: '),
         sg.Text(64 * ' ', key='valmistumisaika'),
         # sg.Text('|'), sg.Text('Aika: '),
         # sg.Text(64 * ' ', key='päivämäärä')
         ],
        [graph],
        [timebar]
    ]
    table_window = None  # Taulukko-ikkuna
    window = sg.Window('Lämpötilakaavio', layout)
    graph = window['graph']
    prev_x, prev_y, i = 0, 0, 0
    prev_x_for_degrees = 3

    def draw_temp_lines():
        y_value = 10
        while y_value < CANVAS_SIZE[1]:
            graph.draw_line((0, y_value),
                (CANVAS_SIZE[0] + 100000000, y_value),
                color='white',
                width=1
            )
            graph.draw_line((0, y_value - 5),
                (CANVAS_SIZE[0] + 100000000, y_value - 5),
                color='grey',
                width=1
            )
            graph.draw_text(f"{y_value} °C",
                (prev_x_for_degrees, y_value - 2),
                color='white'
            )
            y_value += 10

    event = "Käyttöliittymän Event"

    while event not in ('Quit', sg.WIN_CLOSED) and main_thread_state != 'exit':
        event, values = window.read(timeout=5)

        if event == 'View saved values' and table_window is None:
            if len(filtered_data) > 0:
                table_layout = [
                    [
                        sg.Button('<', button_color=('white', 'grey')),
                        sg.Text(24 * ' ', key='lämmitys_kerrat'),
                        sg.Button('>', button_color=('white', 'grey'))
                    ],
                    [
                        sg.Table(filtered_data,
                         background_color='black',
                         headings=taulu_headings,
                         justification='center',
                         key='-TABLE-')
                    ]
                ]
                table_window = sg.Window("Tallennetut arvot", table_layout, resizable=True, finalize=True)
            else:
                sg.Popup('Tietokannasta ei löytynyt lämpötila-arvoja', keep_on_top=True)

        if table_window:
            t_event, t_values = table_window.read(timeout=5)

            if t_event is sg.WIN_CLOSED:
                table_window.close()
                table_window = None
                current_page = 0
            else:
                if t_event == '<' and filtered_data[len(filtered_data) -1][1] > 1:
                    current_page += 1
                    new_data_page = get_pageable_rows(current_page)
                    filtered_data = []
                    for data in new_data_page:
                        new_data = list(deepcopy(data))
                        new_data[2] = datetime \
                            .fromtimestamp(float(data[2])) \
                            .strftime('%Y-%m-%d %H:%M:%S')
                        filtered_data.append(new_data)

                if t_event == '>' and current_page > 0:
                    current_page -= 1
                    new_data_page = get_pageable_rows(current_page)
                    filtered_data = []
                    for data in new_data_page:
                        new_data = list(deepcopy(data))
                        new_data[2] = datetime \
                            .fromtimestamp(float(data[2])) \
                            .strftime('%Y-%m-%d %H:%M:%S')
                        filtered_data.append(new_data)

                table_window['-TABLE-'].update(filtered_data)
                table_window['-TABLE-'].expand(True, True)
                table_window['-TABLE-'].table_frame.pack(expand=True, fill='both')
                try:
                    alku = filtered_data[0][1]
                    viimeisin = filtered_data[len(filtered_data) -1][1]
                    kerrat = f"Kerrat: {viimeisin} - {alku}"
                    table_window['lämmitys_kerrat'].update(kerrat)
                except IndexError:
                    pass

        # Aikaleima
        now = datetime.now()
        now_time = now.strftime("%H:%M:%S")

        # Nucleolla mitattu lämpötila-arvo
        lampotila_arvo = shared_data['current-temp']

        pt_values[i] = lampotila_arvo
        pt_times[i] = str(now_time)

        if lampotila_arvo != shared_data['previews-temp']:
            # Päivitetään vain, jos lämpötila on muuttunut.
            window['output'].update(lampotila_arvo)
            # window['päivämäärä'].update(now.strftime('%Y-%m-%d %H:%M:%S'))

            # Haetaan arvioitu valmistumisaika:
            temp_step = 2.5 * round(lampotila_arvo / 2.5)
            try:
                average_time_left = average_time_left_map[temp_step]
            except KeyError:
                continue

            if average_time_left is not None:
                time_left = get_hours_minutes_seconds(average_time_left)
                window['valmistumisaika'].update(time_left)

            if lampotila_arvo > SAMPLE_MAX:
                lampotila_arvo = SAMPLE_MAX

            if lampotila_arvo < SAMPLE_MIN:
                lampotila_arvo = SAMPLE_MIN

            new_x, new_y = i, lampotila_arvo

            if i >= SAMPLES:
                graph.move(-STEP_SIZE, 0)
                prev_x = prev_x - STEP_SIZE
                # prev_x_for_degrees = STEP_SIZE
                for i in range(SAMPLES):
                    pt_values[i] = pt_values[i + 1]
                    pt_times[i] = pt_times[i + 1]
            else:
                draw_temp_lines()  # Piirretään lämpötila-asteikon apuviivat.

            graph.draw_line((prev_x, prev_y), (new_x, new_y), color='orangered', width=2)

            prev_x, prev_y = new_x, new_y
            i += STEP_SIZE if i < SAMPLES else 0

            timebar.erase()
            time_x = i
            timebar.draw_text(text=str(now_time), location=((time_x - 2), 7))

            if i >= SAMPLES:
                timebar.draw_text(text=pt_times[int(SAMPLES * 0.25)], location=((int(time_x * 0.25) - 2), 7))
                timebar.draw_text(text=pt_times[int(SAMPLES * 0.50)], location=((int(time_x * 0.50) - 2), 7))
                timebar.draw_text(text=pt_times[int(SAMPLES * 0.75)], location=((int(time_x * 0.75) - 2), 7))
            if i > 10:
                timebar.draw_text(text=pt_times[1], location=(2, 7))

        # Tallennetaan nykyinen arvo seruaavaa kierrosta varten:
        shared_data['previews-temp'] = lampotila_arvo

    window.close()


def start_interface(initial_data, kerta, average_time_left_map):
    thread = Thread(target=graph_event_loop,
                    args=(initial_data, kerta, average_time_left_map))
    thread.start()


