import yaml
import serial

file = open('app.yml', 'r')
muuttujat = yaml.load(file, Loader=yaml.FullLoader)
serial_dev = muuttujat['serial']['dev']
file.close()

ser = serial.Serial(serial_dev)
ser.baudrate = 9600


def get_serial_data():
    if ser.is_open:
        '70.0;1 tunti 33 minuuttia ;...;...\n'
        data = ser.readline()
        string = data.decode("utf-8")
        # Tähän voi muuttaa vastaanotettavan datan muotoa
        return string.strip().split(';')
    else:
        raise Exception("Yhteys katkesi!")


