# Raspberry asetus: https://youtu.be/0kpGcMjpDcw
import ssl
import yaml
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from src.sauna_utils import *

file = open('app.yml', 'r')
muuttujat = yaml.load(file, Loader=yaml.FullLoader)
SERVER = muuttujat['server']['domain']
PORT = int(muuttujat['server']['port'])
smtp_user = muuttujat['smtp']['user']
smtp_pass = muuttujat['smtp']['pass']
to_addr = muuttujat['smtp']['to']
from_addr = smtp_user
file.close()

ssl_context = ssl.create_default_context()


def send_email(values, saunan_tila, average_time_left):
    if saunan_tila == "Pian valmis":
        with open('resources/sauna_time_left.html', 'r') as html:
            time_left = get_hours_minutes_seconds(average_time_left)
            email_body = html.read()\
                .replace("{time_left}", time_left)\
                .replace("{temp}", values[0])\
                .replace("{time}", values[1])

    elif saunan_tila == "Sauna Valmis":
        with open('resources/sauna_ready.html', 'r') as html:
            email_body = html.read()\
                .replace("{temp}", values[0])\
                .replace("{time}", values[1])

    elif saunan_tila == "Sauna yliajalla":
        with open('resources/sauna_overtime.html', 'r') as html:
            email_body = html.read()\
                .replace("{temp}", values[0])\
                .replace("{time}", values[1])


    else:
        email_body = "Sauna ei ole vielä valmis."

    with smtplib.SMTP_SSL(SERVER, PORT, context=ssl_context) as smtp:
        message = MIMEMultipart('alternative')
        message['subject'] = "RaspberryPI: " + saunan_tila
        message['To'] = to_addr
        message['From'] = from_addr
        message.attach(MIMEText(email_body, 'html'))

        smtp.login(smtp_user, smtp_pass)
        smtp.sendmail(from_addr, to_addr, message.as_string())
        print("Ilmoitus lähetetty: " + saunan_tila)


