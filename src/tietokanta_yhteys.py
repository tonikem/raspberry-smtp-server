import sqlite3
import time
import yaml
import datetime

file = open('app.yml', 'r')
muuttujat = yaml.load(file, Loader=yaml.FullLoader)
DB_PATH = muuttujat['tietokanta']['polku']
ALUSTA = str(muuttujat['tietokanta']['alusta'])
file.close()

# Kerralla haettavat lämmityskerrat:
PAGE_SIZE = 5


def init_db_connection():
    connection = sqlite3.connect(DB_PATH)
    cursor = connection.cursor()

    if ALUSTA.lower() == 'true':
        print("kanta alustettu!")
        cursor.execute("DROP TABLE IF EXISTS mittaukset")

    cursor.execute("""
            CREATE TABLE IF NOT EXISTS mittaukset (
                _id INTEGER PRIMARY KEY,
                kerta INTEGER NOT NULL,
                aikaleima TEXT NOT NULL,
                lampotila TEXT NOT NULL,
                kulunut_aika TEXT NOT NULL,
                joku_arvo_3 TEXT,
                joku_arvo_4 TEXT
            )
    """)
    connection.commit()


def save_to_db(_time, values, kerta):
    connection = sqlite3.connect(DB_PATH)
    cursor = connection.cursor()
    if _time is None or _time == '':
        _time = str(time.time())

    cursor.execute(f"""
                INSERT INTO mittaukset(
                    kerta,
                    aikaleima,
                    lampotila,
                    kulunut_aika,
                    joku_arvo_3,
                    joku_arvo_4
                )
                VALUES(
                    {kerta},
                    '{_time}',
                    '{values[0]}',
                    '{values[1]}',
                    '{values[2]}',
                    '{values[3]}'
                )
        """)
    connection.commit()


def get_all_rows():
    connection = sqlite3.connect(DB_PATH)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM mittaukset ORDER BY _id DESC")
    connection.commit()
    return cursor.fetchall()


def get_pageable_rows(page=0):
    connection = sqlite3.connect(DB_PATH)
    cursor = connection.cursor()
    cursor.execute("SELECT MAX(kerta) FROM mittaukset")
    max_kerta = cursor.fetchall()[0][0]
    if max_kerta is None:
        return ()
    viimeisin_kerta = max_kerta - (page * PAGE_SIZE)
    alku = viimeisin_kerta - PAGE_SIZE
    cursor.execute(f"""
        SELECT * FROM mittaukset
        WHERE kerta BETWEEN {alku} AND {viimeisin_kerta}
        ORDER BY _id DESC
    """)
    return cursor.fetchall()


