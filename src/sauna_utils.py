

def get_hours_minutes_seconds(aika_jaljella):
    tunnit_minuutit_sekunnit = ""

    if aika_jaljella is None:
        return tunnit_minuutit_sekunnit

    tunnit = aika_jaljella // 3600
    jaannossekunnit = aika_jaljella % 3600
    minuutit = jaannossekunnit // 60
    sekunnit = jaannossekunnit % 60

    if tunnit > 0:
        tunnit_minuutit_sekunnit += str(int(tunnit))
        tunnit_minuutit_sekunnit += f" tunti{(tunnit > 1) * 'a'} "

    tunnit_minuutit_sekunnit += str(int(minuutit))
    tunnit_minuutit_sekunnit += " minuuttia "
    tunnit_minuutit_sekunnit += str(int(sekunnit))
    tunnit_minuutit_sekunnit += " sekunttia "
    return tunnit_minuutit_sekunnit



